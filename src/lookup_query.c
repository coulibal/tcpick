/*
 * lookup_query.c -- the host lookup engine
 * Part of the tcpick project
 *
 * Author: Francesco Stablum <duskdruid @ despammed.com>
 *
 * Copyright (C) 2003, 2004 Francesco Stablum
 * Licensed under the GPL
 *
 */

/* 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at you option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111,
 * USA.
 */

#include "tcpick.h"
#include "extern.h"

void lookupPortname(u_int16_t port, char *result, int bufferSize);
char * lookup_new(struct in_addr ia);
char * lookup(struct in_addr ia);


char *
lookup_new(struct in_addr ia)
{
	register struct hostent * he;
	register struct _l_node * node;
	
	he = gethostbyaddr((char *)&ia, sizeof(struct in_addr), AF_INET);
	
	node = (struct _l_node *)_l_alloc(ia, (he == NULL) ? inet_ntoa(ia) : he->h_name );

	_l_insert( node );

	return node->name;
}

char *
lookup(struct in_addr ia)
/* WARNING: returned buffer is by default static! */

{
	register char * name;

	if(flags.lookup == 0) /* this is default */
		return inet_ntoa(ia);
	
	
	name = (char *)_l_get(ia);

	return ( (name == NULL)
		 ? (char *)lookup_new(ia) 
		 : (char *)name );
}

void lookupPortname(u_int16_t port, char *result, int bufferSize)
{
	struct servent * ptr;
	ptr= getservbyport(port, NULL);
	
	if(ptr!=NULL)
	{
		strncpy(result,ptr->s_name,bufferSize-1);
	}
	else 
	{
		strncpy(result,"NULL",bufferSize-1);
		result[bufferSize-1]='\0';
	}
	
}


