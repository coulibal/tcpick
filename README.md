# Quick Start

## Requirements

for the code to compile you will need `GCC` and `LIBPCAP`
to install libpcap you could use this command:
```bash
$ sudo apt-get install libpcap-dev
```
## Project Compilation

when the code is downloaded to compile it you need :
* first execute the configure script that will generate the makefile and check if that all dependancies are already installed
```bash
coulibaly@ubuntu:~/Desktop/tcpick$ ./configure 
```
* next you have to compile the code by doing

```bash
coulibaly@ubuntu:~/Desktop/tcpick$ make 
``` 
* finally to get the executable you have to issu this command with **`sudo`**:

```bash
coulibaly@ubuntu:~/Desktop/tcpick$ sudo make install
```
the executable will found in **`/usr/local/bin/`**

***Nb: you need root privelege to execute the code.  In case of you did a big updates in the code or just some corruption of the compiled file you have to first issue the command bellow and restart from step1***
```bash
$ sudo make install 
```

### More info

for more information about code compiling or code execution with different flags please refer to the files provided in the gitlab:
* `EXAMPLES` 
* `INSTALL` 

# Initial Code download and compilation

After downloading the tcpick [source code v0.2.1](https://sourceforge.net/projects/tcpick/files/tcpick/0.2.1/), to make the code compile and ran properly we had to fix some very timing consuming bugs.
bug1 and bug2 are required to be fixed in order to be able to run the executable properly.
according to the official doc, there was no trace of this kind of problem, so we supposed that it might be related to **GCC** versions?
The old code has been tested on older versions of GCC `GCC version <= 3.3.3` (2005) and this new one was compiled and tested on  `GCC version 9.3.0`

## Bug1 

### Description

![5b5001f83533311cf265e0e6867e2d5e.png](../../_resources/13d8823748a8456ab25d21eb3bdb7acd.png)
### fix

the fix was to declare function signatures in the respective problematic files. 

## Bug2

### Description

![dca4a8b81df960aff674d3bf6783f458.png](../../_resources/b1b88760c58c448fac06c51228b022b0.png)

![e267e8b3e6d5df9b733f3c5357b48a99.png](../../_resources/4d40358e1a754058ba95738d7c32a799.png)

**display.c**

we managed to retrace the program crash  to the function `char *getportname()`, in fact for each time there  is an attempt to do a conversion between u_int64_t to a char* this problem occurs so the quick workaround and the fixes are: 

```c
//File display.c
[.....]
//line 110
//the code tried to print two string but getportname failed the conversion hence the segmentation fault 
color( c_CLIENTNAME_STATUS, out, " %s:%s ",
		     client_name,
		     getportname(ntohs(conn->client.port)) );
[.....]			 
```
### workaround

since client.port is an int64_t  one way to print to the console is to use this but this work only when you did not activate the protocol name resolver flag `-a` :
```c
getportname(conn->client.port);
	color( c_CLIENTNAME_STATUS, out, " %s:%" PRId64 " ",
		     client_name,
		     ntohs(conn->client.port));
``` 

### fix

rewrited the `getportname()`  to `void lookupPortname(u_int16_t port, char *result, int bufferSize)` function to focus only on the protocol name resolution and added a new functions in `display.c: printIPPORT() & printPortOnly()` to handle the printing to what we wish to do.


# Meta data's 

for adding meta data we've implemented a very intuitive function `void addMeta(struct CONN * conn_ptr, struct HOST_DESC *desc, int buflen )` in `write.c` to make it more scalable in the future when someone else wants to add more metadata he/she will just have to add the parts that he/she wants.
But when there is some more preprocessing to do instead of just picking one value from the IP FILE DESCRIPTOR, you can check as an example for the implementation of the duration meta value on how to do some preprocessing before writing it in the metafile.
when executing the program with **`-wb`** flag we will create a new file in addition to the others created by the older version of tcpick.
meta file will be saved with the `.meta` extension.
meta added so far are for example:
|id|duration|fraglen|packet_size|first_packet_seq|flag|
|- | -      |  -    | -         | -              | -  |
13|1.000000|0|594|284100646|ESTABLISHED
13|1.000000|150|51|284100646|ESTABLISHED
13|1.000000|150|177|284100646|ESTABLISHED
13|1.000000|150|243|284100646|ESTABLISHED
13|1.000000|219|38|284100646|ESTABLISHED
13|1.000000|476|689|284100646|ESTABLISHED

**eg:**
```bash
root@ubuntu:/usr/local/bin# ./tcpick  -C  -i ens33  -wb 
``` 
**result:**
after sniffing some packets you'll found under the same directory of the executable files with .meta files
![ec2e2ce0e2df8a39056e6154e8aef4cb.png](../../_resources/8bf6403575984b50a9e954672310b46e.png)

![4edb9ce6e1281d958456b3a5d5cf80ff.png](../../_resources/22b69fcdd1d14052b6a5f61319b62d00.png)

# Fragmentation policies

the difficulties here when implementing different fragmentations method was the fact that for different Operating System they exist a different way to do the fragmentation so different vendors interpreted the RFC in different ways and the problem was that there was not much source code on how they have done it the only book found was this written by [W. Richard Stevens & Gary R. Wright](https://www.amazon.com/TCP-IP-Illustrated-Implementation-Vol/dp/020163354X) and it is not free.
So we have read different articles and tried to make sense of them and implemented our own way to do fragmentation according to the documentation.
the article used for the fragmentation policies are :

[team haboob Figure4: How would each operating system reassemble fragmented packets](https://dl.packetstormsecurity.net/papers/general/overlapping-ipfragments.pdf)
[ip fragmentation in detail](https://packetpushers.net/ip-fragmentation-in-detail/)
[RFC815 alternative method of reassembling fragments](https://datatracker.ietf.org/doc/html/rfc815)

## Implemented policy

 To be able to rebuild fragments we need to know : **packet_identifier, flags, fragment offset**
 
## BSD left-trimmed Policy

### how it works ?

from what we've understood :

* For each new arrived fragments we need to check first the offset if it is greater or less than the current fragments
* If it is less than the current fragment we will loop through the left part of our list
* If the new offset is < loop(current) and < loop(current+1) : we add a new fragment to our list at loop(current-1)
* Else if the new offset is > loop(current) and  < loop(current+1): we will replace loop(current) by a new value.  

you could test it by issuing the flag **`--BSDPolicy`** command for eg:

```bash
root@ubuntu:/usr/local/bin# ./tcpick  -C  -i ens33  -wb --BSDPolicy  "port 443"
Starting tcpick 0.2.1 at 2021-06-25 13:32 PDT
Timeout for connections is 600
tcpick: listening on ens33
setting filter: "port 443"

``` 
